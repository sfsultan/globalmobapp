import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the GlobalData provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class GlobalData {

  perpage:number = 10;
  query:string;
  url:string = "http://globalapi.loc/api/v1/product"

  constructor(public http: Http) {}

  load(start:number=0):Promise<[any]> {

    return new Promise(resolve => {
      this.http.get(this.url + '?limit=' + this.perpage + '&skip=' + start)
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        });
    });

  }

  searchData(start:number=0, query:string=''):Promise<[any]> {

    console.log("Search Data Function");
    console.log(query);
    console.log(start);

    return new Promise(resolve => {
      this.http.get(this.url + '?limit=' + this.perpage + '&skip=' + start + '&q=' + query)
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        });
    });
  }

}
