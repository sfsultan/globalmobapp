import { FormControl } from '@angular/forms';

export class AgreeValidator {

    static isValid(control: FormControl): any {

        if(!control.value){
            return {
                "validateCheckbox": true
            };
        }
        return null;
    }

}
