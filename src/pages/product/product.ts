import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { GlobalData } from '../../providers/global-data';
import { LoadingController } from 'ionic-angular';

/*
  Generated class for the Product page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
  providers:[GlobalData]
})
export class ProductPage {
  public products:any = [];
  public query:string;
  private start:number=0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public globalService: GlobalData, public loadingCtrl: LoadingController) {
    this.loadProducts();
  }

  loadProducts() {

    return new Promise(resolve => {
      this.globalService.load(this.start)
      .then(data => {
        console.log(data);
        for(let product of data) {
          this.products.push(product);
        }
        resolve(true);
      });
    });
  }

  getItems(ev: any) {

    // set val to the value of the searchbar
    let val = ev.target.value;
    this.products = [];
    this.presentLoading();
    console.log(val.toLowerCase());
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.query = val.toLowerCase();
      return new Promise(resolve => {
        this.globalService.searchData(this.start, this.query)
        .then(data => {
          console.log(data);
          for(let product of data) {
            this.products.push(product);
          }
          resolve(true);
        });
      });
    }
  }

  doInfinite(infiniteScroll:any) {
    console.log('Begin async operation: ' + this.start);
    this.start+=50;

    this.loadProducts().then(()=>{
       infiniteScroll.complete();
     });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductPage');
  }

  presentLoading() {
    this.loadingCtrl.create({
      content: 'Please wait...',
      duration: 2000,
      dismissOnPageChange: true
    }).present();
  }

  openProductDetail(id) {

  }

  // dismissLoading() {
  //   this.loadingCtrl.dismiss();
  // }

}
