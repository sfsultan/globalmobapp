import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { ContactPage } from '../pages/contact/contact';
import { ProductPage } from '../pages/product/product';
import { AboutPage } from '../pages/about/about';
import { HomePage } from '../pages/home/home';
import { OrderPage } from '../pages/order/order';
import { OrderTab1Page } from '../pages/order-tab1/order-tab1';
import { OrderTab2Page } from '../pages/order-tab2/order-tab2';

import { PrivacyPage } from '../pages/privacy/privacy';
import { TermsPage } from '../pages/terms/terms';
import { FaqPage } from '../pages/faq/faq';

@NgModule({
  declarations: [
    MyApp,
    ContactPage,
    ProductPage,
    AboutPage,
    HomePage,
    OrderPage,
    OrderTab1Page,
    OrderTab2Page,
    PrivacyPage,
    TermsPage,
    FaqPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ContactPage,
    ProductPage,
    AboutPage,
    HomePage,
    OrderPage,
    OrderTab1Page,
    OrderTab2Page,
    PrivacyPage,
    TermsPage,
    FaqPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
